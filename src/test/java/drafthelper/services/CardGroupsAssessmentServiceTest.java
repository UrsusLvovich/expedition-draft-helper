package drafthelper.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import drafthelper.entity.CardGroup;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class CardGroupsAssessmentServiceTest {
    @Test
    public void test() {
        Set<CardGroup> cardGroupSet = new HashSet<>();
        CardGroup a = new CardGroup(80);
        CardGroup b = new CardGroup(90);
        CardGroup c = new CardGroup(50);
        cardGroupSet.add(a);
        cardGroupSet.add(b);
        cardGroupSet.add(c);

        Optional<Double> maxScore = cardGroupSet.stream()
                .map(cardGroup -> cardGroup.getScore())
                .max(Double::compare);
        assertEquals(90, maxScore.get());
    }

    @Test
    public void recommendedCardGroupTest() {
        Set<CardGroup> cardGroupSet = new HashSet<>();
        CardGroup middle = new CardGroup(80);
        CardGroup expected = new CardGroup(90);
        CardGroup low = new CardGroup(50);
        cardGroupSet.add(middle);
        cardGroupSet.add(expected);
        cardGroupSet.add(low);

        CardGroupsAssessmentService cardGroupsAssessmentService = new CardGroupsAssessmentService();
        CardGroup actual = cardGroupsAssessmentService.recommendedCardGroup(cardGroupSet);

        assertEquals(expected, actual);
    }
}
