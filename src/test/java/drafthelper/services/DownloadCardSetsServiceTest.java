package drafthelper.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class DownloadCardSetsServiceTest {
    @Test
    public void readConfigFileTest_validConfigFilePresent() {

    }

    @Test
    public void readConfigFileTest_invalidConfigFilePresent() {

    }

    @Test
    public void readConfigFileTest_configFileNotPresent() {

    }

    @Test
    public void downloadCardSsetsTest_cardSetsMapEmpty() {

    }

    @Test
    public void downloadCardSetsTest_resourcePathEmpty() {

    }

    @Test
    public void downloadCardSetsTest_downloadsAndUnzipZipfile() {

    }

    @Test
    public void createCardsMap_resourcesPathEmpty() {

    }

    @Test
    public void createCardsMap_addsToCardMap() {

    }

    @Test
    public void findDataFiles_directoryNotFound() {

    }

    @Test
    public void findDataFiles_returnsDataFile() {

    }

    @Test
    public void cleanupFiles_resourcesPathNotFound() {
    
    }

    @Test
    public void cleanupFiles_resourcesPathEmpty() {

    }

    @Test
    public void cleanupFiles_removesAllFiles() {

    }
    
    @Test
    public void runTest_callsNecessaryFunctions() {

    }
}
