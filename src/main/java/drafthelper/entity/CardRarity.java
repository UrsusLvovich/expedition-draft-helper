package drafthelper.entity;

public enum CardRarity {
    COMMON, RARE, EPIC, CHAMPION;

    private String rarity;
    private CardRarity() {
        this.rarity = null;
    }
    private CardRarity(String rarity) {
        this.rarity = rarity;
    }
    public String getRarity() {
        return this.rarity;
    }
    public static CardRarity toEnum(String cardRarity){
        switch(cardRarity){
            case "COMMON":
                return CardRarity.COMMON;
            case "RARE":
                return CardRarity.RARE;
            case "EPIC":
                return CardRarity.EPIC;
            case "CHAMPION":
                return CardRarity.CHAMPION;
            default:
                return null;
        }
    }

    public static Double rarityValue(String cardRarity){
        switch(cardRarity){
            case "COMMON":
                return 1.0;
            case "RARE":
                return 2.0;
            case "EPIC":
                return 3.0;
            case "CHAMPION":
                return 5.0;
            default:
                return 0.0;
        }
    }
}
