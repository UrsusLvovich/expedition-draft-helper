package drafthelper.entity;

public enum CardType {
    UNIT, SPELL, LANDMARK, ABILITY, TRAP;

    public static CardType toEnum(String type) {
        switch (type) {
            case "Spell":
                return CardType.SPELL;
            case "Unit":
                return CardType.UNIT;
            case "Landmark":
                return CardType.LANDMARK;
            case "Ability":
                return CardType.ABILITY;
            case "Trap":
                return CardType.TRAP;
            default:
                return null;
        }
    }
}
