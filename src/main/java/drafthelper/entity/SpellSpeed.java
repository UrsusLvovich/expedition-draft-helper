package drafthelper.entity;

public enum SpellSpeed {
    SLOW, FAST, BURST;

    private String speed;
    private SpellSpeed() {
        this.speed = null;
    }
    private SpellSpeed(String speed) {
        this.speed = speed;
    }
    public String getSpeed() {
        return this.speed;
    }
    public static SpellSpeed toEnum(String spellSpeed){
        switch(spellSpeed){
            case "SLOW":
                return SpellSpeed.SLOW;
            case "FAST":
                return SpellSpeed.FAST;
            case "BURST":
                return SpellSpeed.BURST;
            default:
                return null;
        }
    }

    public static Double spellValue(String spellSpeed){
        switch(spellSpeed){
            case "SLOW":
                return 1.0;
            case "FAST":
                return 3.0;
            case "BURST":
                return 5.0;
            default:
                return 0.0;
        }
    }
}
