package drafthelper.entity;

import java.util.HashMap;
import drafthelper.entity.Card;

public class CardsMap {
    private static CardsMap cardsMapInstance = new CardsMap();

    private CardsMap() { }

    public static CardsMap getInstance() {
        return cardsMapInstance;
    }

    private HashMap<String, Card> cardsMap;

    public void setCardsMap(HashMap<String, Card> newCardsMap) {
        this.cardsMap = newCardsMap;
    }

    public HashMap<String, Card> getCardsMap() {
        return this.cardsMap;
    }
}
