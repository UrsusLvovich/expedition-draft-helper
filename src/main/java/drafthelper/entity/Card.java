package drafthelper.entity;

import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;


/**
 * Card object to represent a Legends of Runeterra card.
 */
public class Card {
    private String cardCode;
    private CardRarity rarity;
    private Optional<SpellSpeed> speed;
    private Optional<Double> hp;
    private Optional<Double> attack;
    private double cost;
    private String name;
    private String cardId;
    private CardType type;
    private String region;
    private double score;

    /**
     * Constructor.
     *
     * @param cardCode - Unique code for the card
     * @param rarity - The rarity of the card
     * @param speed - The spell speed of the card
     * @param hp - Hit points of a card.
     * @param attack - Attack points of a card.
     * @param cost - Cost of a card.
     * @param name - The name of a card.
     * @param cardId - The Identification of a card.
     * @param type - The type of a card.
     * @param region - The region of a card.
     * @param score - The score of a card which is used to help determine which card group to pick.
     */
    public Card(
            String cardCode,
            CardRarity rarity,
            Optional<SpellSpeed> speed,
            Optional<Double> hp,
            Optional<Double> attack,
            double cost,
            String name,
            String cardId,
            CardType type,
            String region,
            double score) {
        requireNonNull(cardCode);
        requireNonNull(name);
        requireNonNull(cardId);
        requireNonNull(type);
        requireNonNull(region);

        checkArgument(type != CardType.UNIT || (type == CardType.UNIT && hp.isPresent() && attack.isPresent()));

        this.cardCode = cardCode;
        this.rarity = rarity;
        this.speed = speed;
        this.hp = hp;
        this.attack = attack;
        this.cost = cost;
        this.name = name;
        this.cardId = cardId;
        this.type = type;
        this.region = region;
        this.score = score;
    }

    /**
     * @return - Get the Card Code
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * @return - Get the Card rarity
     */
    public CardRarity getRarity() {
        return rarity;
    }

    /**
     * @return - Get the spell speed
     */
    public Optional<SpellSpeed> getSpeed() {
        return speed;
    }

    /**
     * @return - The hit points of this card.
     */
    public Optional<Double> getHp() {
        return hp;
    }

    /**
     * @return - The attack of this card.
     */
    public Optional<Double> getAttack() {
        return attack;
    }

    /**
     * @return - The cost of this card.
     */
    public double getCost() {
        return cost;
    }

    /**
     * @return - The name of this card.
     */
    public String getName() {
        return name;
    }

    /**
     * @return - The card identification of this card.
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * @return - The type of this card.
     */
    public CardType getType() {
        return type;
    }

    /**
     * @return - The region of a card.
     */
    public String getRegion() {
        return region;
    }

    /**
     * @return - The score of a card which is used to help determine which card group to pick.
     */
    public double getScore() {
        return score;
    }

    /**
     * @return - Set the Card Code.
     */
    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    /**
     * @return - Set the Card rarity.
     */
    public void setRarity(CardRarity rarity) {
        this.rarity = rarity;
    }

    /**
     * @return - Set the Spell speed.
     */
    public void set(SpellSpeed speed) {
        this.speed = Optional.of(speed);
    }

    /**
     * @param hp - The hit point of this card.
     */
    public void setHp(double hp) {
        this.hp = Optional.of(hp);
    }

    /**
     * @param attack - The attack of this card.
     */
    public void setAttack(double attack) {
        this.attack = Optional.of(attack);
    }

    /**
     * @param cost - The cost of this card.
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @param name - The name of this card.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param cardId - The cardId of this card.
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * @param type - The type of this card.
     */
    public void setType(CardType type) {
        this.type = type;
    }

    /**
     * @param region - The region of this card.
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @param score - This is to get the cards score.
     */
    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Name: " + name + "\n" + "Attack: " + attack + "\n" + "HP: " + hp + "\n" + "Card ID: " + cardId;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }

        if(object instanceof Card) {
            Card other = (Card) object;
            return this.getCardCode().equals(other.getCardCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(attack, hp, cardId, cost, region, type, score, name);
    }

    /**
     * The builder used to construct a {@link Card}.
     * 
     * Note MATT: Do we need CardBuilder class? Annoying to maintain 2 sets of parameters
     */
    public static class CardBuilder {
        private String cardCode;
        private Optional<Double> hp;
        private Optional<Double> attack;
        private double cost;
        private String name;
        private String cardId;
        private CardType type;
        private String region;
        private double score;

        public CardBuilder setCardCode(String cardCode){
            this.cardCode = cardCode;
            return this;
        }
        public CardBuilder setHp(Optional<Double> hp) {
            this.hp = hp;
            return this;
        }

        public CardBuilder setAttack(Optional<Double> attack) {
            this.attack = attack;
            return this;
        }

        public CardBuilder setCost(int cost) {
            this.cost = cost;
            return this;
        }

        public CardBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CardBuilder setCardId(String cardId) {
            this.cardId = cardId;
            return this;
        }

        public CardBuilder setType(CardType type) {
            this.type = type;
            return this;
        }

        public CardBuilder setRegion(String region) {
            this.region = region;
            return this;
        }

        public CardBuilder setScore(double score) {
            this.score = score;
            return this;
        }

        // Note MATT: Don't really feel like maintaining this right now
        // public Card build() {
        //     return new Card(cardCode, hp, attack, cost, name, cardId, type, region, score);
        // }
    }
}

