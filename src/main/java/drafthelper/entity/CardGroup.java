package drafthelper.entity;

import java.util.HashSet;
import java.util.Set;

class TooManyCardsException extends Exception{
    TooManyCardsException(String S){
        super();
    }
}

/**
 * Object that will represent each card group you pick from
 */
public class CardGroup {
    private double score = 0 ;
    Set<Card> cardGroupSet = new HashSet<>();
    public CardGroup(double score) {
        this.score = score;
    }
    public double getScore() {
        return score;
    }

    /**
     * Adds cards to the group if the group contains less than 3 cards
     *
     * @param newCard - The card being added to the deck
     * @throws TooManyCardsException - If for any reason it adds cards to a deck size of 3, It will yell at you to suck my ass
     */
    public void addCard(Card newCard) throws TooManyCardsException {
        if (cardGroupSet.size()<3){
            cardGroupSet.add(newCard);
        }
        else {
            throw new TooManyCardsException("There are too many cards in this group! (You can suck my ass)");
        }
    }

    /**
     * Returns the cards in the current set.
     *
     * @return - Set of Cards
     */
    public Set<Card> getCardGroup(){
        return cardGroupSet;
    }

    /**
     * Adds the calculated score to the total in the set.
     *
     * @param score - Calculated score to be added
     */
    public void addScore(double score) {
        this.score =+score;
    }
}
