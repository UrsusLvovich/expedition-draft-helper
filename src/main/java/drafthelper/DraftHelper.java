package drafthelper;

import java.io.FileNotFoundException;
import drafthelper.services.DownloadCardSetsService;

public class DraftHelper {

    public static void main(String[] args) throws FileNotFoundException {
        DownloadCardSetsService.run("src/main/java/drafthelper/resources/config.yaml", "src/main/java/drafthelper/resources");
        /**
         * Most of these steps are going to be inside other services:
         * -Ping the runeterra api and wait for the draft phase to begin
         *      Can test using a different endpoint. So won't have to waste draft tickets
         * -Read the screen for the current groups
         *      I think the runeterra api has coordinates for each card
         *      Have to scrape the page, I think, for the actual card
         * -Build up card groups, access score, and return score
         *      Call CardGroupsAssessmentService assessCardGroups
         *      All text responses for now
         *      Maybe something more complex later down the line
         * -When a group is chosen (I think the API returns the chosen group), build up a current chosen deck to further enhance score assessment
         * -Repeat until all groups are chosen
         */
    }
}
