package drafthelper.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import drafthelper.entity.Card;
import drafthelper.entity.CardGroup;
import drafthelper.entity.CardRarity;
import drafthelper.entity.SpellSpeed;

import static com.google.common.base.Preconditions.checkArgument;


public class CardGroupsAssessmentService {
    /**
     * Uses HP, Attack, and Cost to calculate score for board presence
     * Formula : (HP+Attack)/Cost+.5 (.5 is there because of 0 cost cards)
     *
     * @param hp - Health of the card
     * @param attack - Attack of the card
     * @param cost - Cost of the card
     * @return Calculated Score
     *          TODO: Revise each formula for scoring each card
     */
    double assignScoreMonster(Optional<Double> hp, Optional<Double> attack, Double cost) {
        return (hp.get()+attack.get())/(cost+.5);
    }

    /**
     * Calculates the value of a spell cards
     *
     * @param speed - Spell speed
     * @param rarity - Card rarity
     * @param cost - Card cost
     * @return - Calculated score
     *          TODO: Revise formula for spell card effects
     */
    double assignScoreSpell(Optional<SpellSpeed> speed, CardRarity rarity, Double cost) {
        return (SpellSpeed.spellValue(speed.get().getSpeed()) + 
            CardRarity.rarityValue(rarity.getRarity())) / (cost + 0.5);
    }

    /**
     * Calculates the value of a landmark card
     *
     * @param cost
     * @return
     *          TODO: Revise formula for landmark cards
     *          Might have to revisit this once we add the factions
     */
    double assignScoreLandmark(Double cost) {
        return cost/2;
    }

    /** 
     * Calculates the value of an ability card
     * 
     *      TODO: The Ability card is denoted by the term "Skill" in the "keywords" list 
     * @return
    */
    double assignScoreAbility() {
        return 1;
    }

    /**
     * Calculates the value of a trap card
     * @return 
     * 
     *      TODO: "Trap" is captured in the "keywords" list
     */
    double assignScoreTrap() {
        return 1;
    }

    /**
     * Goes through the list of card groups
     * Assigns scores for each card
     * Adds each card score to the total of the card group
     *
     * @param cardGroups - Three card groups that a user can choose from to be weighted.
     * @param deck - The current deck of cards the user has constructed.
     *             TODO: Integrate deck list into scoring. Take curve into account, spell amount, monsters amount, etc.
     */
    // TODO: Complete Functions, Write a test for this
    public CardGroup assessCardGroups(Set<CardGroup> cardGroups, List<Card> deck) {
        for (CardGroup group : cardGroups) {
            for (Card card : group.getCardGroup()) {
                switch(card.getType()) {
                    // NOTE MGE: I really don't like how this is layed out. But this might be the best way to get the score right now
                    // unless we  move this into CardType?
                    case UNIT:
                        card.setScore(assignScoreMonster(card.getHp(), card.getAttack(), card.getCost()));
                    case SPELL:
                        card.setScore(assignScoreSpell(card.getSpeed(), card.getRarity(), card.getCost()));
                    case LANDMARK:
                        card.setScore(assignScoreLandmark(card.getCost()));
                    case ABILITY:
                        card.setScore(assignScoreAbility());
                    case TRAP:
                        card.setScore(assignScoreTrap());
                }
                group.addScore(card.getScore());
            }
        }
        return recommendedCardGroup(cardGroups);
    }

    /**
     * Compares the scores of each card group and returns the best
     *
     * @param cardGroups - The set of cards to find the best value
     * @return The card group with the best score
     */
    public CardGroup recommendedCardGroup(Set<CardGroup> cardGroups) {
        checkArgument(cardGroups.size() == 3 || cardGroups.size() == 2);

        CardGroup bestGroup = cardGroups.iterator().next();
        for (CardGroup group : cardGroups) {
             if (group.getScore() > bestGroup.getScore()) {
                 bestGroup = group;
             }
        }
        return bestGroup;
    }

}
