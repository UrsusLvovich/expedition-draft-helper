package drafthelper.services;

// library to read yaml files
import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.Reader;

import javax.net.ssl.HttpsURLConnection;
import java.net.URL;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

import com.google.gson.Gson;
import drafthelper.entity.Card;
import drafthelper.entity.CardsMap;
import drafthelper.entity.CardType;
import drafthelper.entity.CardRarity;
import drafthelper.entity.SpellSpeed;

import org.apache.commons.io.FileUtils;

/**
 * Service to download card sets and store the card data in a singleton HashMap.
 */
public class DownloadCardSetsService {

    /**
     * Reads from the given configPath, and returns the Key-Value pairs from the config
     * @param configPath - path to the yaml config file.
     * @return configMap - the configuration read in from the yaml config file
     * @throws {@link java.io.FileNotFoundException}
     */
    private static HashMap readConfigFile(String configPath) throws FileNotFoundException {
        Yaml yamlReader = new Yaml();
        HashMap configMap = yamlReader.load(new FileInputStream(configPath));
        return configMap;
    }

    /**
     * Reads the urls from the card sets config
     * Downloads each card sets' zip file to the resources/cardsets directory
     * Unzips each zip file
     * @param cardSetsMap - map from card set to url
     * @param resourcePath - path to resources folder
     */
    private static void downloadCardSets(HashMap<String, String> cardSetsMap, String resourcePath) {
        for (String setName : cardSetsMap.keySet()) {
            try {
                URL urlObj = new URL(cardSetsMap.get(setName));
                HttpsURLConnection con = (HttpsURLConnection) urlObj.openConnection();

                InputStream stream = con.getInputStream();
                Path sourcePath = Paths.get(resourcePath, "cardsets", String.format("%s.zip", setName));
                Path destPath = Paths.get(resourcePath, "cardsets", setName);
                Files.copy(stream, sourcePath);
                unzip(sourcePath.toString(), destPath.toString());
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Unzips file at zipFilePath to destDirectory
     * @param zipFilePath - path to the zip file
     * @param destDir - destination directory
     */
    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
                byte[] bytesIn = new byte[1024];
                int read = 0;
                while ((read = zipIn.read(bytesIn)) != -1) {
                    bos.write(bytesIn, 0, read);
                }
                bos.close();
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    /**
     * Read from the data files in the resources/cardsets/{setName}/en_us/data/setX-en_us.json folder
     * Build the master card map with key = card id and value = card
     * Set the card map in the singleton {@link drafthelper.entity.CardsMap} class
     * @param resourcesPath - path to resources folder
     */
    private static void createCardsMap(String resourcesPath) {
        ArrayList<File> dataFiles = new ArrayList<>();
        findDataFiles(Paths.get(resourcesPath, "cardsets").toString(), dataFiles);

        HashMap<String, Card> masterCardMap = new HashMap<>();
        try {
            Gson gson = new Gson();
            for (File file : dataFiles) {
                Reader reader = Files.newBufferedReader(file.toPath());
                List<Map<?, ?>> cardsListMap = gson.fromJson(reader, List.class);

                for (Map<?, ?> cardMap : cardsListMap) {
                    Card card = new Card(
                        (String) cardMap.get("cardCode"),
                        (CardRarity) cardMap.get("rarity"),
                        Optional.of((SpellSpeed) cardMap.get("spellSpeed")),
                        Optional.of((double) cardMap.get("health")),
                        Optional.of((double) cardMap.get("attack")),
                        (Double) cardMap.get("cost"),
                        (String) cardMap.get("name"),
                        (String) cardMap.get("cardCode"),
                        CardType.toEnum((String) cardMap.get("type")),
                        (String) cardMap.get("region"),
                        0);

                    masterCardMap.put(card.getCardId(), card);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        CardsMap.getInstance().setCardsMap(masterCardMap);
    }

    /**
     * Finds each card set data json file
     * @param directoryName - directory to search from
     * @param files - recursive list building from the file names that match the regex pattern
     */
    private static void findDataFiles(String directoryName, List<File> files) {
        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] dataFileList = directory.listFiles();
        Pattern pattern = Pattern.compile("set[0-9]+-en_us\\.json");
        if(dataFileList != null)  {
            for (File file : dataFileList) {
                if (file.isFile()) {
                    Matcher matcher = pattern.matcher(file.getName());
                    if (matcher.find()) {
                        files.add(file);
                    }
                } else if (file.isDirectory()) {
                    findDataFiles(file.getAbsolutePath(), files);
                }
            }
        }
    }

    /**
     * Deletes all the card data
     * @param resourcesPath - path to resources folder
     */
    private static void cleanupFiles(String resourcesPath) {
        try {
            FileUtils.cleanDirectory(Paths.get(resourcesPath, "cardsets").toFile());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Steps for DownloadCardSetsService
     * 1. Read from config where to look for card sets
     * 2. Download all card sets from the urls
     * 3. create the CardsList object, singleton, list of all cards
      * @param configPath
      */
     public static void run(String configPath, String resourcesPath) throws FileNotFoundException {
        HashMap config = readConfigFile(configPath);
        downloadCardSets((HashMap<String, String>) config.get("cardSets"), resourcesPath);
        createCardsMap(resourcesPath);
        cleanupFiles(resourcesPath);
     }
}
