Expedition Draft Helper isn't endorsed by Riot Games and doesn't reflect the views or opinions of Riot Games or anyone officially involved in producing or managing Riot Games properties. Riot Games, and all associated properties are trademarks or registered trademarks of Riot Games, Inc.

We're going to use this README for development notes. We can update it to be an actual README later.

# Expedition Draft Helper

Legends of Runeterra expedition draft helper

Gives recommendations on available cards in the expedition draft phase.

# Getting Started
- gitbash for Windows (Optional)
- IDE (IntelliJ, Eclipse, VSCode)
- gradle
- JDK 15

# Overview of the draft helper
- java application that when ran, search for / waits for the Legends of Runeterra running process
- Utilize the [RiotGames Legends of Runeterra Developer api](https://developer.riotgames.com/docs/lor)
- Refer to the [Expedition wiki page](https://leagueoflegends.fandom.com/wiki/Expedition_(Legends_of_Runeterra)) for specifics on the game mode 
- Wait for the player to go into an Expedition Draft
- Read in the next available cards for the draft
    - Keep a record of cards already chosen
    - Based on some criteria, give a recommendation on what card group to choose next

# Little More Specific notes
- RiotGames LoR api
    - Currently doesn't have support for DraftPicks
    - We'll have to keep an eye for when this becomes available
    - The information our application pulls from the draft will need to change based on what's available from the api

- Testing the DraftHelper application
    - We can make our own version of the draft so we don't have to waste arena entries
    - Grab all available card information from the api
        - We'd want to always pull card information in case new cards are release / some existing cards are changed
    - Create our own draft where we choose 3 card groups at a time and keep track of the cards we've already chosen in our deck
    - Let the application run on this draft information

- Choosing card criteria
    - Look at card information such as Cost, HP, Attack, Region, Card Type, Effects
        - having logic around Effects is going to be a lot harder. Can either do this last, for not include it.
    - Look at cards already chosen
    - Build logic around best cards to choose
        - Would we have to have "meta picks" in mind for the first couple cards?
        - If we don't have enough data yet in the draft phase, we'd have to go by what card's "strongest" and that can be very different depending on meta.
        - Would need to assign different weights for different categories of card information.
    - Ideally, we'd want the application to give each of the next 3 cards a percentage on how well it thinks the card fits into the current deck.

# Resources for criteria
- https://runetiera.com/
- https://www.ftpbust.com/legends-of-runeterra-expedition-tier-list/
- https://runeterraccg.com/expeditions-archetypes/

# First iteration
- Application generates card groupings of 3, and gives a score for each.
- Criteria starts out averaging HP and Attack
- Application reads in card information from given URLs in the RiotGames docs.
    - Could be specified in a config file
    - Store card information in memory
- Application outputs in command line
